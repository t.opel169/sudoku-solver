﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public enum Difficulty
    {
        Easy,
        VeryHard
    }

    public class SudokuModel
    {
        private Node[,] Board { get; set; }
        private int RemainingNodes { get; set; }
        private readonly int[,] EasyBoard1 =
        {
            { 0, 1, 0, 8, 0, 2, 3, 7, 0 },
            { 0, 0, 3, 0, 0, 0, 0, 5, 0 },
            { 0, 8, 0, 0, 7, 0, 0, 0, 0 },
            { 5, 0, 0, 7, 6, 0, 0, 4, 0 },
            { 0, 0, 1, 0, 0, 0, 0, 0, 7 },
            { 0, 0, 6, 1, 9, 0, 0, 2, 8 },
            { 0, 0, 2, 6, 0, 0, 1, 0, 4 },
            { 0, 0, 4, 0, 8, 0, 7, 0, 0 },
            { 6, 3, 0, 9, 0, 1, 2, 0, 0 },
        };
        private readonly int[,] VeryHardBoard1 =
        {
            { 4, 0, 8, 0, 6, 0, 0, 0, 0 },
            { 0, 0, 0, 1, 0, 0, 7, 0, 2 },
            { 0, 0, 5, 0, 0, 0, 6, 3, 0 },
            { 0, 0, 6, 0, 5, 0, 0, 0, 4 },
            { 0, 8, 0, 2, 0, 0, 3, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 1 },
            { 6, 0, 0, 0, 0, 0, 0, 7, 0 },
            { 0, 0, 4, 3, 0, 0, 0, 5, 0 },
            { 0, 5, 0, 0, 4, 2, 0, 0, 0 },
        };

        public SudokuModel(Difficulty difficulty)
        {
            Board = new Node[9, 9];
            if (difficulty == Difficulty.Easy)
            {
                SetBoard(EasyBoard1);
            }
            else
            {
                SetBoard(VeryHardBoard1);
            }
        }

        private void SetBoard(int [,] board)
        {
            for (var x = 0; x < 9; x++)
            {
                for (var y = 0; y < 9; y++)
                {
                    Board[y, x] = new Node();
                    var value = board[x, y];
                    if (value == 0) RemainingNodes++;
                    SetNodeValue(y, x, value);
                }
            }
        }

        public void SetNodeValue(int x, int y, int value)
        {
            Board[x, y].Value = value;
        }

        public int? GetNodeValue(int x, int y)
        {
            return Board[x, y].Value;
        }

        public bool CanSolveNext()
        {
            for (var x = 0; x < 9; x++)
            {
                for (var y = 0; y < 9; y++)
                {
                    // Value already filled
                    if (Board[x, y].Value != 0) continue;
                    var valuesRemaining = new HashSet<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9};
                    for (var lineIndex = 0; lineIndex < 9; lineIndex++)
                    {
                        var value = Board[lineIndex, y].Value;
                        if (value == 0 || !valuesRemaining.Contains(value)) continue;
                        valuesRemaining.Remove(value);
                    }
                    for (var columnIndex = 0; columnIndex < 9; columnIndex++)
                    {
                        var value = Board[x, columnIndex].Value;
                        if (value == 0 || !valuesRemaining.Contains(value)) continue;
                        valuesRemaining.Remove(value);
                    }
                    CheckNeighbourGrid(x, y, valuesRemaining);

                    // One value left means we have found solution for the node
                    if (valuesRemaining.Count == 1)
                    {
                        Board[x, y].Value = valuesRemaining.First();
                        RemainingNodes--;
                        var isSolved = ((RemainingNodes == 0) ? true : false);
                        return !isSolved;
                    }
                    else
                    {
                        Board[x, y].AddPossibleValues(valuesRemaining);
                    }
                }
            }

            // TODO: Solve base on possible values, not working currently
            for (var x = 0; x < 9; x++)
            {
                for (var y = 0; y < 9; y++)
                {
                    // Value already filled
                    if (Board[x, y].Value != 0) continue;
                    var possibleValues = Board[x, y].PossibleValues;
                    for (var lineIndex = 0; lineIndex < 9; lineIndex++)
                    {
                        var lineNode = Board[lineIndex, y];
                        if (lineIndex != x && lineNode.Value == 0 && lineNode.PossibleValues.SetEquals(possibleValues))
                        {
                            // Might use recursion here? Need to go through line again
                            for (var index = 0; index < 9; index++)
                            {
                                var node = Board[index, y];
                                if (index != x && index != lineIndex && node.Value == 0)
                                {
                                    node.PossibleValues.ExceptWith(possibleValues);
                                    if (node.PossibleValues.Count == 1)
                                    {
                                        node.Value = node.PossibleValues.First();
                                        RemainingNodes--;
                                        var isSolved = ((RemainingNodes == 0) ? true : false);
                                        return !isSolved;
                                    }
                                }
                            }
                        }   
                    }
                    for (var columnIndex = 0; columnIndex < 9; columnIndex++)
                    {
                        var lineNode = Board[x, columnIndex];
                        if (columnIndex != y && lineNode.Value == 0 && lineNode.PossibleValues.SetEquals(possibleValues))
                        {
                            // Might use recursion here? Need to go through column again
                            for (var index = 0; index < 9; index++)
                            {
                                var node = Board[x, index];
                                if (index != y && index != columnIndex && node.Value == 0)
                                {
                                    node.PossibleValues.ExceptWith(possibleValues);
                                    if (node.PossibleValues.Count == 1)
                                    {
                                        node.Value = node.PossibleValues.First();
                                        RemainingNodes--;
                                        var isSolved = ((RemainingNodes == 0) ? true : false);
                                        return !isSolved;
                                    }
                                }
                            }
                        }
                    }
                    //        for (var columnIndex = 0; columnIndex < 9; columnIndex++)
                    //{
                    //    var value = Board[x, columnIndex].Value;
                    //    if (value == 0 || !valuesRemaining.Contains(value)) continue;
                    //    valuesRemaining.Remove(value);
                    //}
                }
            }

            // Unable to solve current board
            return false;
        }

        private void CheckNeighbourGrid(int x, int y, HashSet<int> valuesRemaining)
        {
            var gridStartX = x / 3 * 3;
            var gridStartY = y / 3 * 3;
            for (var i = gridStartX; i < gridStartX + 3; i++)
            {
                for (var z = gridStartY; z < gridStartY + 3; z++)
                {
                    var value = Board[i, z].Value;
                    if (value == 0 || !valuesRemaining.Contains(value)) continue;
                    valuesRemaining.Remove(value);
                }
            }
        }
    }
}
