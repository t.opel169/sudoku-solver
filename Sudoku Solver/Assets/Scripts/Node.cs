﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class Node
    {
        public int Value { get; set; }
        public HashSet<int> PossibleValues { get; set; }

        public Node()
        {
            PossibleValues = new HashSet<int>();
        }

        public void AddPossibleValues(HashSet<int> values)
        {
            PossibleValues.UnionWith(values);
        }
    }
}
