﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SudokuBoard : MonoBehaviour
{
    private SudokuModel SudokuModel { get; set; }
    private const int BOARD_SIZE = 9;

    private void Start()
    {
        SudokuModel = new SudokuModel(Difficulty.VeryHard);
        StartCoroutine(SolveNext());
    }

    private void OnGUI()
    {
        DrawBoard();
    }

    private void DrawBoard()
    {
        // TODO: Very inneficient, need to redraw only updated tiles
        for (var x = 0; x < BOARD_SIZE; x++)
        {
            for (var y = 0; y < BOARD_SIZE; y++)
            {
                var textStyle = new GUIStyle { fontSize = 50, alignment = TextAnchor.MiddleCenter };
                var nodeValue = SudokuModel.GetNodeValue(x, y);
                GUI.Box(new Rect(new Vector2(x * 80, y * 80), new Vector2(80, 80)),
                    nodeValue == 0 ? "-" : nodeValue.ToString(), textStyle);
            }
        }
    }

    private IEnumerator SolveNext()
    {
        var isSolved = false;
        while (!isSolved)
        {
            isSolved = !SudokuModel.CanSolveNext();
            yield return new WaitForSeconds(.2f);
        }
        Debug.Log("Solving finished");
    }
}
